module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'АО «Шувалово» - мясоперерабатывающее предприятие',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          '«Шувалово» – это крупнейшее мясоперерабатывающее предприятие  в Костромской области замкнутого цикла со своей сырьевой базой, успешно работающее на рынке вот уже более 45 лет.'
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#ffca0e' },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/styles/style.scss',
    '~/assets/styles/three-dots.scss',
    'element-ui/lib/theme-chalk/index.css',
    '@fortawesome/fontawesome-svg-core/styles.css',
    '~/assets/styles/fonts.scss',
    'quill/dist/quill.snow.css',
    'quill/dist/quill.bubble.css',
    'quill/dist/quill.core.css',
    'swiper/dist/css/swiper.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/ElementUi',
    '~/plugins/Fontawesome',
    { src: '~/plugins/VueViewports', ssr: false },
    { src: '~/plugins/Shave', ssr: false },
    { src: '~/plugins/directives/ImageLazyLoad', ssr: false },
    { src: '~/plugins/directives/Scroll', ssr: false },
    { src: '~plugins/NuxtQuillPlugin', ssr: false },
    { src: '~plugins/YandexMaps', ssr: false },
    { src: '~plugins/VMask', ssr: false },
    { src: '~/plugins/Swiper', ssr: false }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    ['@nuxtjs/moment', { locales: ['ru'], defaultLocale: 'ru' }],
    ['vue-scrollto/nuxt', { duration: 300 }]
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    baseURL:
      process.env.NODE_ENV === 'production'
        ? 'https://api-shuvalovo.tw1.ru'
        : 'http://127.0.0.1:5000'
  },
  router: {
    scrollBehavior: function(to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition;
      } else {
        return { x: 0, y: 0 };
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    postcss: [
      require('autoprefixer')({
        browsers: ['> 1%', 'last 2 versions', 'not ie <= 8']
      })
    ],
    extractCSS: true,
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push(
          {
            enforce: 'pre',
            test: /\.(js|vue)$/,
            loader: 'eslint-loader',
            exclude: /(node_modules)/
          },
          {
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /(node_modules)/
          }
        );
      }
    }
  }
};
