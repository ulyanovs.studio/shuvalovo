module.exports = {
  presets: ['vue'],
  plugins: [
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-transform-classes',
    '@babel/plugin-transform-runtime',
    'transform-es2015-constants'
  ]
};
