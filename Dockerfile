FROM node:carbon AS base

RUN mkdir -p /root/shuvalovo
COPY . /root/shuvalovo

WORKDIR /root/shuvalovo

COPY package*.json ./

RUN npm ci && npm i -g pm2

ENV NODE_ENV=production

COPY . /root/shuvalovo
RUN npm run build

ENV HOST 0.0.0.0
EXPOSE 3000

CMD pm2 startOrRestart ecosystem.json --no-daemon
