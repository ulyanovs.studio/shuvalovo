let markerIcon = {
  layout: 'default#image',
  imageHref: 'img/logo.png',
  imageSize: [80, 40],
  imageOffset: [-40, -20]
};

module.exports = [
  {
    id: 1,
    coords: [57.802387, 40.997151],
    balloon: {
      header: 'ул.Костромская',
      body: 'ул.Костромская,84 (тонар)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 2,
    coords: [57.774862, 40.944405],
    balloon: {
      header: 'Калиновский рынок',
      body: 'Калиновский рынок, колбасный ряд; павильоны: 9,12',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 3,
    coords: [57.767978, 40.923696],
    balloon: {
      header: 'Центральный рынок',
      body: 'Центральный рынок, колбасный ряд',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 4,
    coords: [57.772876, 40.971962],
    balloon: {
      header: 'ул.Шагова',
      body: 'ул.Шагова, 197А (киоск)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 5,
    coords: [57.741548, 41.00305],
    balloon: {
      header: 'м/р-н Давыдовский-1',
      body: 'м/р-н Давыдовский-1 (мини-рынок)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 6,
    coords: [57.732368, 41.007051],
    balloon: {
      header: 'м/р-н Давыдовский-2',
      body: 'м/р-н Давыдовский-2, д.34',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 7,
    coords: [57.750926, 40.904382],
    balloon: {
      header: 'ул.Беленогова',
      body: 'ул. Беленогова, 17 (тонар)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 8,
    coords: [57.751103, 40.911281],
    balloon: {
      header: 'ул.Голубкова',
      body: 'ул. Голубкова, 5 (магазин)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 10,
    coords: [57.80243, 40.919591],
    balloon: {
      header: 'ул.Боровая',
      body: 'ул. Боровая, 35 (магазин)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 11,
    coords: [57.779907, 40.921908],
    balloon: {
      header: 'ул.Козуева',
      body: 'ул. Козуева, 54/1 (магазин)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 12,
    coords: [57.769622, 40.923085],
    balloon: {
      header: 'пр-т.Текстильщиков',
      body: 'пр-т. Текстильщиков, 3 (магазин)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 13,
    coords: [57.759094, 40.977227],
    balloon: {
      header: 'ул.Никитская',
      body: 'ул. Никитская, 142 ( магазин, напротив ж/д вокзала)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 14,
    coords: [57.749013, 41.001352],
    balloon: {
      header: 'п.Октябрьский',
      body: 'п. Октябрьский (мини-рынок)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 15,
    coords: [57.736832, 40.910078],
    balloon: {
      header: 'м/р-н Паново',
      body: 'м/р-н Паново, 11 (тонар)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 16,
    coords: [57.78043, 40.953233],
    balloon: {
      header: 'пр-т Мира',
      body: 'пр-т Мира, 129 (остановочный комплекс)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 17,
    coords: [57.783487, 40.940827],
    balloon: {
      header: 'ул. Ленина',
      body: 'ул. Ленина, 92 (магазин)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 18,
    coords: [57.509423, 41.239777],
    balloon: {
      header: 'п.Красное-на-Волге',
      body: 'п.Красное-на-Волге ( тонар)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 19,
    coords: [57.735415, 41.065405],
    balloon: {
      header: 'п.Караваево',
      body: 'п.Караваево, ул.Штеймана, 58 (тонар)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 20,
    coords: [57.746536, 40.96492],
    balloon: {
      header: 'м/р-н Черноречье',
      body: 'м/р-н Черноречье, 29 (магазин)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 21,
    coords: [57.459808, 40.552746],
    balloon: {
      header: 'г.Нерехта',
      body: 'г.Нерехта (магазин на территории рынка)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 22,
    coords: [57.690597, 41.027972],
    balloon: {
      header: 'п.Минское',
      body: 'п.Минское',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 23,
    coords: [57.806396, 41.10362],
    balloon: {
      header: 'п.Никольское',
      body: 'п.Никольское',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 24,
    coords: [57.820046, 40.830101],
    balloon: {
      header: 'п.Яковлевское',
      body: 'п.Яковлевское',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 25,
    coords: [57.810808, 40.805657],
    balloon: {
      header: 'п.Шунга',
      body: 'п.Шунга',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  },
  {
    id: 26,
    coords: [57.714744, 41.15353],
    balloon: {
      header: 'с.Гридино',
      body: 'с.Гридино (автомагазин)',
      footer: 'Телефон: 8(4942)66-90-34'
    },
    icon: markerIcon
  }
];
