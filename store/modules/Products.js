const state = {
  categories: null
};

const getters = {
  CATEGORIES: state => state.categories
};

const mutations = {
  SET_CATEGORIES: (state, payload) => {
    state.categories = payload;
  }
};

const actions = {};

export default {
  state,
  getters,
  mutations,
  actions
};
