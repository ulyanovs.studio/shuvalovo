import Vuex from 'vuex';

// import store components
import products from './modules/Products';

const store = () =>
  new Vuex.Store({
    state: {
      loading: false,
      pages: [],
      headingTitle: '',
      publicLink: '',
      menuColor: '',
      pagination: {},
      id: null,
      user: null,
      userAuth: false,
      adminAuth: false,
      toolbarNotPhotoVideo: [
        ['bold', 'italic', 'underline', 'strike'],
        [{ align: [] }],
        [{ header: [2, 3, 4, 5, 6, false] }],
        ['blockquote', 'link'],
        [{ list: 'ordered' }, { list: 'bullet' }],
        [{ indent: '-1' }, { indent: '+1' }],
        [{ color: [] }, { background: [] }],
        ['clean']
      ],
      baseURL:
        process.env.NODE_ENV === 'production'
          ? 'https://api-shuvalovo.tw1.ru'
          : 'http://127.0.0.1:5000'
    },
    getters: {
      USER: state => state.user,
      LOADING: state => state.loading
    },
    mutations: {
      pages(state, payload) {
        state.pages = payload;
      },
      id(state, payload) {
        state.id = payload;
      },
      headingTitle(state, payload) {
        state.headingTitle = payload;
      },
      publicLink(state, payload) {
        state.publicLink = payload;
      },
      pagination(state, payload) {
        state.pagination = payload;
      },
      userAuth(state, payload) {
        state.userAuth = payload;
      },
      adminAuth(state, payload) {
        state.adminAuth = payload;
      },
      user(state, payload) {
        state.user = payload;
      },
      SET_LOADING(state, payload) {
        state.loading = payload;
      }
    },
    actions: {},
    modules: {
      products
    }
  });
export default store;
