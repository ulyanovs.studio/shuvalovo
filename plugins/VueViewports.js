import Vue from 'vue';

// import $currentViewport function
import VueViewports from 'vue-viewports';

// custom options by $currentViewport property
const options = [
  {
    rule: '320px',
    label: 'mobile'
  },
  {
    rule: '768px',
    label: 'tablet'
  },
  {
    rule: '1140px',
    label: 'desktop-medium'
  },
  {
    rule: '1521px',
    label: 'desktop-large'
  },
  {
    rule: '1920px',
    label: 'hd-desktop'
  },
  {
    rule: '2560px',
    label: 'qhd-desktop'
  },
  {
    rule: '3840px',
    label: 'uhd-desktop'
  }
];

Vue.use(VueViewports, options);
