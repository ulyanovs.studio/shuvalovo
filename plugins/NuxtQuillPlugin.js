import Vue from 'vue';
import VueQuillEditor, { Quill } from 'vue-quill-editor/dist/ssr';
import { ImageUpload } from './ImageUploadQuill';

Quill.register('modules/imageUpload', ImageUpload);

Vue.use(VueQuillEditor);
